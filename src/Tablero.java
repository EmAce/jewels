/*Hecho por:
  Enrique Enciso de Le�n A00367589
  Emmanuel Acevedo Parra A01226082
*/

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import javax.swing.ImageIcon;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

import java.io.File;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;



public class Tablero extends JPanel implements MouseListener{
	private Joya[][] joyas;
	private Image fondo;
	private int x = 222,
				punt =0,
				y=126,
				xf,
				yf,
				mx,
				my,
				xi,
				yi;
	
	private boolean click=false,
					audiocheck=true;
	private String win ="";
	private  Clip sonido;
	
	public Tablero(Joya array[][]) {
		this();
		this.joyas =  array;
		
	}	
		
	
	public Tablero( ){
		
		this.joyas = new Joya[8][8];
		for (int i=0;i<8;i++){
			for (int j=0;j<8;j++){
				int ram = (int) (Math.random() * 7);
				try {
					boolean cheker = true;	
					while(cheker){
							if ((this.joyas[i][j-2].getTipo() != ram) && (this.joyas[i-2][j].getTipo() != ram)) {
								this.joyas[i][j] = new Joya(ram); 
								cheker=false;
							}
							else{
								ram = (int) (Math.random() * 7);
							}
					}
				}
				catch(Exception exception){
					try{
						boolean cheker1 =true;
						while(cheker1){
							if (this.joyas[i-2][j].getTipo() != ram){
								this.joyas[i][j] = new Joya(ram);
								cheker1=false;
							}
							else{
								ram = (int) (Math.random() * 7);
							}
						}
					}
					catch(Exception exception1){
						this.joyas[i][j] = new Joya(ram);
					}
			   }
			}
		}
		this.setSize(771, 499);
		this.setPreferredSize(new Dimension(760, 488));
		this.addMouseListener(this);
		this.setVisible(true);
		 try {
	            sonido=AudioSystem.getClip();
	            sonido.open(AudioSystem.getAudioInputStream(new File("haha.wav")));
	    		sonido.loop(-1);
	        } catch (Exception e) {
	            System.out.println("" + e);
	        }
	
		
	}
	public void paint(Graphics g){

		super.paint(g);
		Color cuadro = new Color(255,25,38);
		Color score = new Color (204,200,20);
		//Color azul_1 =new Color (0,164,255);
		Font ganar=new Font("TimesRoman", Font.BOLD, 50);
		this.fondo=new ImageIcon("tablero2.png").getImage();
		g.drawImage(this.fondo, 0, 0, null);
		g.setColor(score);
		g.drawString("score: "+punt,440,70);
		g.setFont(ganar);
		g.drawString(win,300,50);
		if(audiocheck==false){
			g.setColor(cuadro);
			g.drawLine(545, 465, 570, 450);
		}
		for (int i=0;i<8;i++){
			for (int j=0;j<8;j++){
				this.joyas[i][j].pinta(g,x,y);
				this.x = this.x+40;
			}
			this.x = 222;
			this.y = this.y +40;
		}
		this.y=126;
		g.setColor(cuadro);
		g.drawRect((mx*40)+222, (my*40)+126, 40, 40);
	}
	
	public void seleccion(int x,int y){   // parametros xi yi
		joyas[y][x].getTipo();
		this.click = true;
	}
	
public void intercambia (int xi, int yi, int xf, int yf){
	//if que permite el cambio con las cuatro joyas juntas y que no sean iguales

	if ( (this.joyas[yf][xf].getTipo() != this.joyas[yi][xi].getTipo()) && (( ( (xi+1==xf) || (xi-1==xf) ) && (yi==yf) ) || ( (yi+1==yf) || (yi-1==yf) ) && (xi==xf)) ) {
		boolean cambio =false;
		// esto cambia las joyas que se seleccionaron 
		
		int tempTipo = joyas[yf][xf].getTipo();	
		joyas[yf][xf]=new Joya(joyas[yi][xi].getTipo());
		joyas[yi][xi]=new Joya(tempTipo);
		
		for (int i=0; i<8;i++){
			for(int j=0; j<8;j++){
				if(j<6&&((joyas[i][j].getTipo()==joyas[i][j+1].getTipo()) && (joyas[i][j].getTipo()==joyas[i][j+2].getTipo()))){
					cambio=true;
					break;
				}
				
				if(j<6&&((joyas[j][i].getTipo()==joyas[j+1][i].getTipo()) && (joyas[j][i].getTipo()==joyas[j+2][i].getTipo()))){
					cambio=true;
					break;
				}
						
					
			}
			if (cambio){
				break;
			}
		
		}
	
	if((!cambio) ){
		tempTipo = joyas[yi][xi].getTipo();	
		joyas[yi][xi]=new Joya(joyas[yf][xf].getTipo());
		joyas[yf][xf]=new Joya(tempTipo);
		
	}
	cambio =false;

}

	this.click=false;
	this.repaint();
	generaJoyas();	
		
		

}//fin del m�todo intercambia
	

	public void generaJoyas(){
		
		boolean check=false;
		
		do{
			check=false;
			for(int i=0;i<8;i++){
				for(int j=0;j<8;j++){
					int cont=0;
					int tmptipo = -2;
					if(j<6 && (joyas[i][j].getTipo() ==joyas[i][j+1].getTipo())&&(joyas[i][j].getTipo() ==joyas[i][j+2].getTipo()) ){
						tmptipo=joyas[i][j].getTipo();
					}
					while(j+cont<8 && (tmptipo== joyas[i][j+cont].getTipo())){
						joyas[i][j+cont] = new Joya(-1);
						cont++;
						this.punt = this.punt +10;
						
					}
					
				}
			}
				for (int i=0; i <8;i++){
					for (int j=0;j<8;j++){
						int cont=0;
						int tmptipo = -2;
							if(j<6 && (joyas[j][i].getTipo() ==joyas[j+1][i].getTipo())&&(joyas[j][i].getTipo() ==joyas[j+2][i].getTipo()) ){
								tmptipo=joyas[j][i].getTipo();
							}
							
							while(j+cont<8 && (tmptipo== joyas[j+cont][i].getTipo())){
								joyas[j+cont][i] = new Joya(-1);
								cont++;
								this.punt = this.punt +10;
							}
					}
				}
			for(int j=7;j>0;j--){
				for(int i=0;i<8;i++){
					if (joyas[j][i].getTipo()==-1){
						for(int filaTmp=j-1;filaTmp>=0;filaTmp--){
							if(joyas[filaTmp][i].getTipo()!=-1){
								joyas[j][i]=joyas[filaTmp][i];
								joyas[filaTmp][i]=new Joya(-1);
								break;
							}
						}
					}
				}
			}	
		
		for(int i=0; i<8;i++){
			for(int j=0; j<8;j++){
				if(joyas[j][i].getTipo()==-1){
					joyas[j][i]= new Joya();
				}
			}
		}
		
		for (int i=0; i<8;i++){
			for(int j=0; j<8;j++){
				if(j<6&&((joyas[i][j].getTipo()==joyas[i][j+1].getTipo()) && (joyas[i][j].getTipo()==joyas[i][j+2].getTipo()))){

					check=true;
					break;
				}
				
				if(j<6&&((joyas[j][i].getTipo()==joyas[j+1][i].getTipo()) && (joyas[j][i].getTipo()==joyas[j+2][i].getTipo()))){
					
					check=true;
					break;
				}
						
					
			}
			if (check){
				break;
			}
		}// end for
		}while(check);
		 
		isWin();		
		this.repaint();
	}
	
	public void isWin(){
		if (punt>=600){
			this.win="GANASTE";
			this.punt=600;
			this.removeMouseListener(this);
		}
		
		
	}
	public static void main(String[] args) {
	    
		Ventana interfaz=new Ventana();
	
		    
		       
		    }

		



	
	public void mouseClicked(MouseEvent e) {
		
		if (this.click == false){
			if(e.getX()>=222 && e.getX()<=540 && e.getY()>=126 && e.getY()<=446){
				this.xi =((e.getX()-222)/40); // rango de 0 a 7
				this.yi = ((e.getY()-126)/40);// rango de 0 a 7
				seleccion(this.xi,this.yi);
				this.mx = this.xi; // para hacer el cuadro
				this.my = this.yi; // para hacer el cuadro
			}
		}
		else {
			if(e.getX()>=222 && e.getX()<=540 && e.getY()>=126 && e.getY()<=446){
				this.xf =((e.getX()-222)/40); // rango de 0 a 7
				this.yf = ((e.getY()-126)/40);// rango de 0 a 7
				intercambia(this.xi,this.yi,this.xf,this.yf);
				this.mx = this.xf;
				this.my = this.yf;
				
			}
		}
		if(e.getX()>=545 && e.getX()<=570 && e.getY()>=450 && e.getY()<=465){
			audiocheck=  !audiocheck;
			if(audiocheck){
				sonido.loop(-1);
				
			}
			else{
				sonido.stop();
			}
		}
		
		if(e.getX()>=490 && e.getX()<545 && e.getY()>=450 && e.getY()<=465){
			
			JOptionPane.showMessageDialog(null, "1.-Mueva las joyas dando un click sobre una joya y posteriormente " +
			 		"repitiendo el click en una  joya adyacente" +
			 		"(Solo se movera si se produce una destrucci�n de tres o m�s fichas). "+"\n"+
			 		"2.-El jugador gana la partida a los 600 puntos." +"\n"+
			 		"3.-Si desea quitar la musica solo pulse la bocina de la esquina.");
			
			}
			
		
		this.repaint();	
		
		
		
		
		
		
		
	}

	public void mouseEntered(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	public void mouseExited(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	
	public void mousePressed(MouseEvent e) {
		
	}

	
	public void mouseReleased(MouseEvent e) {
		
		
	}
}
	