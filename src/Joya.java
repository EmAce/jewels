import java.awt.Graphics;
import java.awt.Image;

import javax.swing.ImageIcon;


public class Joya {
	
	private int tipo;
	// 0 = amarillo, 1 = azul, 2 = morado, 3 = naranja, 4 = plata, 5 = rojo, 6 = verde.
	private  static Image[]  imagenes = {new ImageIcon("amarillo.png").getImage(),  
		                                 new ImageIcon("azul.png").getImage(),
		                                 new ImageIcon("morado.png").getImage(),
		                                 new ImageIcon("naranja.png").getImage(),
		                                 new ImageIcon("plata.png").getImage(),
		                                 new ImageIcon("rojo.png").getImage(),
		                                 new ImageIcon("verde.png").getImage()};
	
	public Joya(){
		this.tipo = (int) (Math.random() * 7);
	}
	public Joya (int x){
		this.tipo = x;
	}
	
     public void  pinta ( Graphics g, int x, int y){
		if (this.tipo>=0){
			g.drawImage(imagenes[tipo], x, y, null);
		}
	}
	
     public int getTipo(){
    	 return this.tipo;
    	 
     }

}
