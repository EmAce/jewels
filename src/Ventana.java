
import javax.swing.JFrame;
import java.awt.BorderLayout;

public class Ventana extends JFrame{
	
	protected Tablero panel;
	
	public Ventana(){
		Joya array[][] =  {{new Joya(0),new Joya(0),new Joya(0),new Joya(5),new Joya(2),new Joya(1),new Joya(1),new Joya(0)},
				           {new Joya(1),new Joya(0),new Joya(1),new Joya(0),new Joya(3),new Joya(0),new Joya(4),new Joya(1)},
				           {new Joya(1),new Joya(3),new Joya(1),new Joya(0),new Joya(1),new Joya(2),new Joya(0),new Joya(5)},
				           {new Joya(1),new Joya(1),new Joya(0),new Joya(1),new Joya(1),new Joya(2),new Joya(4),new Joya(6)},
				           {new Joya(1),new Joya(1),new Joya(2),new Joya(5),new Joya(5),new Joya(1),new Joya(5),new Joya(2)},
				           {new Joya(1),new Joya(2),new Joya(2),new Joya(0),new Joya(5),new Joya(6),new Joya(1),new Joya(4)},
				           {new Joya(0),new Joya(0),new Joya(2),new Joya(3),new Joya(4),new Joya(3),new Joya(1),new Joya(3)},
				           {new Joya(0),new Joya(0),new Joya(6),new Joya(0),new Joya(0),new Joya(0),new Joya(0),new Joya(0)}};
		this.setSize(771, 499);
		
		this.setTitle("Jewels Adventure");
		this.panel=new Tablero();//poner array como argumento 
		this.add(panel,BorderLayout.CENTER);
		this.pack();
		this.setResizable(false);
		this.setVisible(true);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}
	
}

